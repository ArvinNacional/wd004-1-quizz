/**
 * Give at least three reasons why you should learn javascript.
 */

1. To be able to manipulate datas easily.
2. To create an interactive website.
3. Knowing Javascript can make my salary go higher!!

/**
 * In two sentences, state in your own words, what is your understanding about Javascript?
 */

1. Javascript is a progamming language that you can use to manipulate data and content of your website
2. Be able to change the content of the website without reloading the page.

/**
 * What is a variable? Give at least three examples.
 */

1.
Examples:
	1. num1
	2. num2
	3. days

/**
 * What are the data types we talked about today? Give at least two samples each.
 */

===> string, number, boolean

/**
 * Write the correct data type of the following:
 */

1. "Number"  string
2. 158    number
3. pi   undefined
4. false  boolean
5. 'true'  string
6. 'Not a String'  string

/**
 * What are the arithmetic operators?
 */ arithmetic operators lets us do arithmetic operations to our variables.

/**
 * What is a modulo? 
 */ modulo gives us the remainder of two variables

/**
 * Interpret the following code
 */

let number = 4;
number += 4;

Q: What is the value of number? answer: 8

---- 

let number = 4;
number -= 4;

Q: What is the value of number? answer: 0


---- 
let number = 4;
number *= 4;

Q: What is the value of number? answer: 16

---- 

let number = 4;
number /= 4;

Q: What is the value of number? answer: 1

