
let student = "brandon";
// console.log(student);


const day = "Monday";
// console.log(day);

// const is short for constat.
// we cannot reassign a value.

student ="Brenda";
// console.log(student);


console.log(typeof "Am I a string?");
console.log(typeof "1234");
console.log(typeof 1234);
console.log(typeof "true");
console.log(typeof false);


const num1 = 10;
const num2 = 2;

// console.log(num1 + num2);


let output = 0;
const num4 = 5;
const num5 = 8;

output += num4;
console.log(output)
